const db = require('../db')
const sanitizeHtml = require('sanitize-html')

function escapeHtml(text) {
    const map = {
      '&': '&amp;',
      '<': '&lt;',
      '>': '&gt;',
      '"': '&quot;',
      "'": '&#39;'
    };
  
    return text.replace(/[&<>"']/g, (match) => map[match]);
}

const getKomentari = async (req, res) => {
    try {
        const result = await db.pool.query('SELECT * FROM komentari');
        res.json(result.rows);
    } catch (error) {
        console.error('Greška pri dohvaćanju komentara:', error);
        res.status(500).send('Server error');
    }
}

const getKomentar = async (req, res) => {
    console.log("tu sam")
    const komentarID = req.params.komentarID;
    console.log(komentarID)
    try {
        const result = await db.pool.query(`SELECT * FROM komentari WHERE "komentarID" = $1`, [komentarID]);
        res.json(result.rows[0]);
    } catch (error) {
        console.error('Greška pri dohvaćanju komentara:', error);
        res.status(500).send('Server error');
    }
}

const addKomentar = async (req, res) => {
    console.log(req.body)
    const { naslov, tekst } = req.body;
    try {
        const result = await db.pool.query('INSERT INTO komentari (naslov, tekst) VALUES ($1, $2)', [naslov, tekst]);
        res.status(200).send('Komentar dodan');
    } catch (error) {
        console.error('Greška pri dohvaćanju komentara:', error);
        res.status(500).send('Server error');
    }
}

const addSiguranKomentar = async (req, res) => {
    const { naslov, tekst } = req.body;
    const escapedTekst = escapeHtml(tekst);
    console.log(escapedTekst);

    const siguranTekst = sanitizeHtml(escapedTekst, {
        allowedTags: ['b', 'i', 'em', 'strong', 'a'],
        allowedAttributes: {
            'a': ['href']
        },
        allowedIframeHostnames: []
    });

    try {
        const result = await db.pool.query('INSERT INTO komentari (naslov, tekst) VALUES ($1, $2)', [naslov, siguranTekst]);
        res.status(200).send('Komentar sanitiziran i dodan u bazu');
    } catch (error) {
        console.error('Greška pri dohvaćanju komentara:', error);
        res.status(500).send('Server error');
    }
}

const getSiguranKomentar = async (req, res) => {
    const komentarID = req.params.komentarID;
    try {
        const result = await db.pool.query('SELECT * FROM komentari WHERE "komentarID" = $1', [komentarID]);
        const komentar = result.rows[0];
        komentar.tekst = escapeHtml(komentar.tekst);

        komentar.tekst = sanitizeHtml(komentar.tekst, {
            allowedTags: ['b', 'i', 'em', 'strong', 'a'],
            allowedAttributes: {
                'a': ['href']
            },
            allowedIframeHostnames: []
        });

        res.json(komentar);
    } catch (error) {
        console.error('Greška pri dohvaćanju komentara:', error);
        res.status(500).send('Server error');
    }
}

const deleteKomentar = async (req, res) => {
    const komentarID = req.params.komentarID;
    try{
        const result = await db.pool.query('DELETE FROM komentari WHERE "komentarID" = $1', [komentarID]);
        console.log(result.rows)
        res.status(200).send("Obrisano")
    } catch (error) {
        console.log(error)
        res.status(500).send("error")
    }
}

const getAdminKomentari = async (req, res) => {
    try {
        const result = await db.pool.query('SELECT * FROM "adminKomentari"')
        res.json(result.rows);
    } catch (error) {
        console.log(error)
        res.status(500).send("error")
    }
}

const addAdminKomentar = async (req, res) => {
    console.log(req.body)
    const { naslov, tekst } = req.body;
    try {
        const result = await db.pool.query('INSERT INTO "adminKomentari" (naslov, tekst) VALUES ($1, $2)', [naslov, tekst]);
        res.status(200).send('Komentar dodan');
    } catch (error) {
        console.error('Greška pri dohvaćanju komentara:', error);
        res.status(500).send('Server error');
    }
}

module.exports = { getKomentari, getKomentar, addKomentar, addSiguranKomentar, getSiguranKomentar, deleteKomentar, getAdminKomentari, addAdminKomentar }