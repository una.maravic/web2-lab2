const db = require('../db')

const login = async (req, res) => {
    const { username, password } = req.body
    try {
        const result = await db.pool.query('SELECT * FROM users WHERE username = $1 AND password = $2', [username, password]);
        if (result.rowCount === 1) {
            const user = result.rows[0];

            req.session.user = {
                id: user.userID,
                username: user.username,
                role: user.role,
            };
            console.log("session user " + JSON.stringify(req.session.user))
            res.send({loggedIn: true, user: req.session.user})
        } else 
            res.status(404).send('Greška prilikom prijave');
    } catch (error) {
        console.error('Greška pri dohvaćanju komentara:', error);
        res.status(500).send('Server error');
    }
}

const logout = async (req, res) => {
    req.session.user = undefined
    req.session.destroy((err) => {
        if (err) {
            console.error('Greška pri odjavi:', err);
            res.status(500).send('Server error');
        } else {
            res.status(200).send('Odjavljeni ste');
        }
    });
}

const addUser = async (req, res) => {
    const { username, password, role } = req.body;
    try {
        const result = await db.pool.query("INSERT INTO users (username, password, role) VALUES ($1, $2, $3)", [username, password, role])
        res.status(200).send("ok")
    } catch (error) {
        console.log(error)
        res.status(500).send("Error")
    }
}

const getUsers = async (req, res) => {
    try {
        const result = await db.pool.query("SELECT * FROM users")
        res.json(result.rows)
    } catch (error) {
        console.log(error)
        res.status(500).send("Error")
    }
}

const isLoggedIn = async (req, res) => {
    console.log(req.session.user)
    if (req.session.user) {
        res.send({loggedIn: true, user: req.session.user})
    } else {
        res.send({loggedIn: false});
    }
}

const checkRole = async (req, res) => {
    const userID = req.params.userID;
    try {
        const result = await db.pool.query('SELECT "role" FROM users WHERE "userID" = $1', [userID]);
        console.log(result.rows[0].role);
        res.send({role: result.rows[0].role});
    } catch(error) {
        console.log(error);
        res.status(500).send("error");
    }
}

const deleteUser = async (req, res) => {
    const userID = req.params.userID;
    try{
        const result = await db.pool.query('DELETE FROM users WHERE "userID" = $1', [userID]);
        console.log(result.rows)
        res.status(200).send("Obrisano")
    } catch (error) {
        console.log(error)
        res.status(500).send("error")
    }
}

module.exports = { login, logout, addUser, getUsers, isLoggedIn, checkRole, deleteUser }