const db = require('../db')

const getSessions = async (req, res) => {
    try {
        const result = await db.pool.query("SELECT * FROM sessions")
        res.json(result.rows)
    } catch (error) {
        console.log(error)
        res.status(500).send("Error")
    }
}

const deleteSessions = async (req, res) => {
    try {
        await db.pool.query("DELETE FROM sessions")
        res.status(200).send("obrisano")
    } catch (error) {
        console.log(error)
        res.status(500).send("Error")
    }
}

module.exports = { getSessions, deleteSessions }