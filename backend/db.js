const session = require('express-session');
const pgSession = require('connect-pg-simple')(session);
const { Pool } = require('pg');

const pool = new Pool({
  connectionString: process.env.POSTGRES_URL,
  ssl: {
    rejectUnauthorized: false,
  },
});

const execute = async (query) => {
  try {
    const result = await pool.query(query);
    return true;
  } catch (error) {
    console.error(error.stack);
    return false;
  }
};

const text = `
  CREATE TABLE IF NOT EXISTS "users" (
    "userID" SERIAL,
    "username" VARCHAR(100) NOT NULL,
    "password" VARCHAR(100) NOT NULL,
    "role" VARCHAR(15) DEFAULT 'user' NOT NULL,
    PRIMARY KEY ("userID")
  ); 
  CREATE TABLE IF NOT EXISTS "komentari" (
    "komentarID" SERIAL PRIMARY KEY, 
    "naslov" TEXT NOT NULL,
    "tekst" TEXT NOT NULL
  );
  CREATE TABLE IF NOT EXISTS sessions (
    sid VARCHAR NOT NULL PRIMARY KEY,
    sess JSON NOT NULL,
    expire TIMESTAMP(6) NOT NULL
  );
  CREATE TABLE IF NOT EXISTS "adminKomentari" (
    "komentarID" SERIAL PRIMARY KEY, 
    "naslov" TEXT NOT NULL,
    "tekst" TEXT NOT NULL
  );
`;

execute(text).then((result) => {
  if (result) {
    console.log('Table created');
  }
});

module.exports = { pool, pgSession };
