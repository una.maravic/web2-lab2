const express = require('express');
const app = express();
const cors = require('cors');
const bodyParser = require("body-parser");
const cookieParser = require("cookie-parser");
const port = process.env.PORT || 8080;
const session = require('express-session');
require('dotenv').config();
const { pool, pgSession } = require('./db');

const komenatriRouter = require('./router/komentariRouter');
const userRouter = require('./router/userRouter');
const sessionRouter = require('./router/sessionRouter');

app.use(cors({
  origin: ["http://localhost:3000", "https://web2-lab2-front.onrender.com"],
  methods: ["GET", "POST", "DELETE", "PUT"],
  credentials: true
}));
app.use(express.json());
app.use(cookieParser());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(
  session({
    store: new pgSession({
      pool: pool,
      tableName: 'sessions',
    }),
    key: "userID",
    secret: 'web2-lab2',
    resave: false,
    saveUninitialized: false,
    cookie: {
      sameSite: "none",
      secure: "auto",
      expires: new Date(Date.now() + 60 * 60 * 24 * 1000),
      httpOnly: false,
    },
  })
);

app.use('/komentari', komenatriRouter);
app.use('/user', userRouter);
app.use('/session', sessionRouter);

app.listen(port, () => {
  console.log(`Server radi na portu ${port}`);
});
