const express = require('express')
const router = express.Router();
const sessionController = require('../controller/sessionController'); 

router.get('/', sessionController.getSessions);
router.delete('/', sessionController.deleteSessions);

module.exports = router;