const express = require('express')
const router = express.Router();
const komentariController = require('../controller/komentariController'); 

router.get('/', komentariController.getKomentari);
router.get('/:komentarID', komentariController.getKomentar);
router.post('/', komentariController.addKomentar);
router.get('/siguran/:komentarID', komentariController.getSiguranKomentar);
router.post('/siguran/', komentariController.addSiguranKomentar);
router.delete('/:komentarID', komentariController.deleteKomentar);
router.get('/role/admin', komentariController.getAdminKomentari);
router.post('/role/admin', komentariController.addAdminKomentar);

module.exports = router;