const express = require('express')
const router = express.Router();
const userController = require('../controller/userController'); 

router.post('/login', userController.login);
router.post('/addUser', userController.addUser);
router.post('/logout', userController.logout);
router.get('/getUsers', userController.getUsers);
router.get('/login', userController.isLoggedIn);
router.get('/role/:userID', userController.checkRole);
router.delete('/:userID', userController.deleteUser);

module.exports = router;