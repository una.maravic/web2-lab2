import React, { useEffect, useState } from 'react';
import { AppBar, Toolbar, Typography, Button } from '@material-ui/core';
import axios from 'axios';
import { useNavigate, useLocation } from 'react-router-dom';

const Header = () => {
    const navigate = useNavigate();
    const [isLoggedIn, setIsLoggedIn] = useState(false);
    const location = useLocation();

    useEffect(() => {
        if (localStorage.getItem('userID') === "null") setIsLoggedIn(false)
        else setIsLoggedIn(true)
    }, [location.pathname]);

    const handleLogout = async () => {
        try {
        await axios.post('https://web2-lab2-backend-77yw.onrender.com/user/logout');
        document.cookie = "userID=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
        localStorage.setItem('userID', null);
        localStorage.setItem('dobraKontrola', false);
        navigate('/')
        } catch (error) {
        console.error('Greška prilikom odjave:', error);
        }
    };

    const getCookie = (name) => {
        const value = `; ${document.cookie}`;
        const parts = value.split(`; ${name}=`);
        if (parts.length === 2) return parts.pop().split(';').shift();
    };

    return (
        <AppBar position="static" color="primary">
            <Toolbar>
                <Typography variant="h6" style={{ flexGrow: 1 }}>
                Dobrodošli
                </Typography>
                {isLoggedIn && (
                <Button color="inherit" onClick={handleLogout}>
                    Logout
                </Button>
                )}
            </Toolbar>
        </AppBar>
    );
};

export default Header;
