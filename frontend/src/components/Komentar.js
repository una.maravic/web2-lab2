import React, { useState, useEffect } from 'react';
import { Button, Typography, Container, Paper } from '@material-ui/core';
import axios from 'axios';
import { useParams, useNavigate, useLocation } from 'react-router-dom';

const Komentar = () => {
  const { komentarID } = useParams();
  const [komentar, setKomentar] = useState(null);
  const navigate = useNavigate();
  const location = useLocation();
  const sigurno = location.state && location.state.sigurno;
  const divRef = React.useRef(null);
  
  useEffect(() => {
    const fetchKomentar = async () => {
      try {
        console.log(sigurno)
        let endpoint = 'https://web2-lab2-backend-77yw.onrender.com/komentari';
        if (sigurno) {
          endpoint = 'https://web2-lab2-backend-77yw.onrender.com/komentari/siguran';
        }
        const response = await axios.get(`${endpoint}/${komentarID}`);
        setKomentar(response.data);

        divRef.current.innerHTML = response.data.tekst;
      } catch (error) {
        console.error('Greška prilikom dohvaćanja komentara:', error);
      }
    };

    if (komentarID) {
      fetchKomentar();
    }
  }, [komentarID]);

  const handlePovratak = () => {
    navigate('/user/komentari');
  };

  return (
    <>
      <div ref={divRef}></div>
      <Container component="main" maxWidth="sm" style={{ marginTop: '20px' }}>
        <Paper elevation={3} style={{ padding: 20 }}>
          {komentar ? (
            <>
              <Typography variant="h5" gutterBottom>
                {komentar.naslov}
              </Typography>
              <Typography variant="body1" paragraph>
                {komentar.tekst}
              </Typography>
              <Button variant="contained" color="primary" onClick={handlePovratak}>
                Povratak
              </Button>
            </>
          ) : (
            <Typography variant="body1">
              Dohvaćanje komentara...
            </Typography>
          )}
        </Paper>
      </Container>
    </>
  );
};

export default Komentar;
