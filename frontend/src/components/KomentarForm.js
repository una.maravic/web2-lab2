import React, {useEffect, useState} from "react";
import { Grid, TextField, Button, Paper, Typography, List, ListItem, ListItemText, Container } from '@material-ui/core';
import { ToggleButton, ToggleButtonGroup } from "@mui/material";
import axios from "axios";
import { useNavigate } from "react-router-dom";
import { useFormik } from "formik";

const KomentarForm = () => {
  axios.defaults.withCredentials = true;
  const navigate = useNavigate();
  const [komentari, setKomentari] = useState([]);
  const [sigurno, setSigurno] = useState(false);

  const formik = useFormik({
    initialValues: {
      naslov: '',
      tekst: '',
    },
    onSubmit: async (values) => {
      try {
        const serverEndpoint = sigurno
          ? 'https://web2-lab2-backend-77yw.onrender.com/komentari/siguran'
          : 'https://web2-lab2-backend-77yw.onrender.com/komentari';
        const result = await axios.post(serverEndpoint, values);
        console.log('Odgovor od servera:', result.data);
        fetchKomentari();
        formik.resetForm();
      } catch (error) {
        console.error('Greška prilikom slanja komentara:', error);
      }
    },
  });

  const fetchKomentari = async () => {
    try {
      const response = await axios.get('https://web2-lab2-backend-77yw.onrender.com/komentari');
      setKomentari(response.data);
    } catch (error) {
      console.error('Greška prilikom dohvaćanja komentara:', error);
    }
  };

  useEffect(() => {
    fetchKomentari();
  }, []);

  const handleButtonClick = (komentarID) => {
    console.log(`Kliknut komentar s ID-om: ${komentarID}`);
    console.log(sigurno);
    navigate(`/user/komentar/${komentarID}`, { state: { sigurno } });
  };

  return (
    <Container component="main" maxWidth="sm">
      <Paper elevation={3} style={{ padding: 20, marginTop: 60 }}>
        <Typography variant="h6" align="left" gutterBottom>
          Unesi svoj komentar
        </Typography>
        <ToggleButtonGroup
          value={sigurno}
          exclusive
          onChange={() => setSigurno(!sigurno)}
          aria-label="text alignment"
          style={{ marginBottom: 10}}
        >
          <ToggleButton value={false} aria-label="left aligned" color="primary">
            Nesigurno
          </ToggleButton>
          <ToggleButton value={true} aria-label="centered" color="secondary">
            Sigurno
          </ToggleButton>
        </ToggleButtonGroup>
        <form onSubmit={formik.handleSubmit}>
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <TextField
                id="naslov"
                name="naslov"
                label="Naslov"
                fullWidth
                value={formik.values.naslov}
                onChange={formik.handleChange}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                id="tekst"
                name="tekst"
                label="Unesi svoj komentar"
                fullWidth
                value={formik.values.tekst}
                onChange={formik.handleChange}
              />
            </Grid>
            <Grid item xs={12}>
              <Button type="submit" variant="contained" color="primary" fullWidth>
                Pošalji
              </Button>
            </Grid>
          </Grid>
        </form>
      </Paper>
      <Paper elevation={3} style={{ padding: 20, marginTop: 50 }}>
        <Typography variant="h5" gutterBottom>
          Svi komentari:
        </Typography>
        <List>
          {komentari.map((komentar) => (
            <ListItem key={komentar.komentarID}>
              <Button
                variant="contained"
                color="primary"
                onClick={() => handleButtonClick(komentar.komentarID)}
                fullWidth
              >
                <ListItemText primary={komentar.naslov} />
              </Button>
            </ListItem>
          ))}
        </List>
      </Paper>
    </Container>
  );
};

export default KomentarForm;
