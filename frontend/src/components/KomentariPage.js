import React, { useEffect, useState } from 'react';
import KomentarForm from './KomentarForm';
import { Typography, Container} from '@material-ui/core';
import { ToggleButton, ToggleButtonGroup } from '@mui/material';
import axios from 'axios';

const KomentariPage = () => {
  axios.defaults.withCredentials = true;
  const [dobraKontrola, setDobraKontrola] = useState(false);

  const handleToggle = (event, newValue) => {
    if (newValue === null) return
    setDobraKontrola(newValue);
    localStorage.setItem('dobraKontrola', newValue)
  };

  useEffect(() => {
    localStorage.setItem('dobraKontrola', false);
  }, [])
  
  return (
    <Container component="main" maxWidth="sm" style={{ marginTop: '30px' }}>
      <Typography variant="h4" align="center" gutterBottom>
        Komentari
      </Typography>
      <ToggleButtonGroup
        value={dobraKontrola}
        exclusive
        onChange={handleToggle}
        aria-label="text alignment"
        style={{ marginTop: 20, marginLeft: 40 }}
      >
        <ToggleButton value={false} aria-label="left aligned" color="primary">
          Loša kontrola pristupa
        </ToggleButton>
        <ToggleButton value={true} aria-label="centered" color="secondary">
          Dobra kontrola pristupa
        </ToggleButton>
      </ToggleButtonGroup>
      <KomentarForm />
    </Container>
  );
};

export default KomentariPage;
