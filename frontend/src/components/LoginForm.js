import React, { useEffect } from 'react';
import { useFormik } from 'formik';
import { TextField, Button, Container, Paper, Typography } from '@material-ui/core';
import axios, { Axios } from 'axios';
import { useNavigate } from 'react-router-dom';

const LoginForm = () => {
    const navigate = useNavigate();
    axios.defaults.withCredentials = true;
    const formik = useFormik({
        initialValues: {
        username: '',
        password: '',
        },
        onSubmit: async (values) => {
        try {
            const response = await axios.post('https://web2-lab2-backend-77yw.onrender.com/user/login', values);
            console.log(response);
            localStorage.setItem('userID', response.data.user.id);
            navigate('/user/komentari')
        } catch (error) {
            console.error('Greška prilikom prijave:', error);
        }
        },
    });

    useEffect(() => {
        axios.get("https://web2-lab2-backend-77yw.onrender.com/user/login").then((response) => {
            console.log(response);
        })
    }, []);

    return (
        <Container component="main" maxWidth="xs">
        <Paper elevation={3} style={{ padding: 20, marginTop: 100}}>
            <Typography component="h1" variant="h5">
            Login
            </Typography>
            <form onSubmit={formik.handleSubmit}>
            <TextField
                id="username"
                name="username"
                label="Username"
                value={formik.values.username}
                onChange={formik.handleChange}
                margin="normal"
                fullWidth
            />
            <TextField
                id="password"
                name="password"
                type="password"
                label="Password"
                value={formik.values.password}
                onChange={formik.handleChange}
                margin="normal"
                fullWidth
            />
            <Button type="submit" variant="contained" color="primary" fullWidth style={{ marginTop: 20 }}>
                Login
            </Button>
            </form>
        </Paper>
        </Container>
    );
};

export default LoginForm;
