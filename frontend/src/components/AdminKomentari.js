import React, { useEffect, useState } from 'react';
import { Typography, Container, Paper, List, ListItem, Box, Button } from '@material-ui/core';
import axios from 'axios';
import { useNavigate } from 'react-router-dom';

const KomentariPage = () => {
  axios.defaults.withCredentials = true;
  const [komentari, setKomentari] = useState([]);
  const [user, setUser] = useState(true);
  const navigate = useNavigate();

  useEffect(() => {
    const fetchKomentari = async () => {
      try {
        const userID = parseInt(localStorage.getItem('userID'), 10)
        console.log(userID);
        const response = await axios.get(`https://web2-lab2-backend-77yw.onrender.com/user/role/${userID}`);
        if (response.data.role !== 'user') setUser(false)
        console.log(user)
        const adminResponse = await axios.get("https://web2-lab2-backend-77yw.onrender.com/komentari/role/admin");
        setKomentari(adminResponse.data);
      } catch (error) {
        console.error('Greška prilikom dohvaćanja komentara:', error);
      }
    };

    fetchKomentari();
  }, []);

  const handlePovratak = () => {
    navigate('/user/komentari');
  };

  return (
    <Container component="main" maxWidth="sm" style={{ marginTop: '50px', textAlign: 'center' }}>
      <Typography variant="h4" align="center" gutterBottom>
        Komentari za administratore
      </Typography>
      <Paper elevation={3} style={{ padding: 20, marginTop: 50 }}>
        <Typography variant="h5" gutterBottom>
          {(user === true && localStorage.getItem('dobraKontrola') == "true" ? 'Pristup zabranjen' : 'Svi komentari administratora:')}
        </Typography>
        {(user === false || localStorage.getItem('dobraKontrola') == "false") && (
          <List>
            {komentari.map((komentar) => (
              <ListItem key={komentar.komentarID}>
                <Box
                  bgcolor="primary.main"
                  color="white"
                  p={2}
                  borderRadius={4}
                >
                  {komentar.naslov}
                </Box>
              </ListItem>
            ))}
          </List>
        )}
        <Button variant="contained" color="primary" onClick={handlePovratak}>
          Povratak
        </Button>
      </Paper>
    </Container>
  );
};

export default KomentariPage;
