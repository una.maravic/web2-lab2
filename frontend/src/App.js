import './App.css';
import { Routes, Route, Navigate } from 'react-router-dom'
import LoginForm from './components/LoginForm';
import KomentariPage from './components/KomentariPage';
import Komentar from './components/Komentar';
import Layout from './components/Layout';
import AdminKomentari from './components/AdminKomentari';

function App() {
  return (
    <Routes>
      <Route  path="/" element={<Layout />}>
        <Route path="/" element={<LoginForm />} />
        <Route path="/user/komentari" element={<KomentariPage />} /> 
        <Route path="/user/komentar/:komentarID" element={<Komentar />} />
        <Route path="/admin/komentari" element={<AdminKomentari />} /> 
      </Route>
    </Routes>
  );
}

export default App;
